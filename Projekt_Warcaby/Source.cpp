#include <iostream>
#include <string>
#include <algorithm> // for transform

using namespace std;

class Person { //base class
public:
	string name;
	Person(string personName) : name(personName) {}
};

class Field {
public:
	int xDim;
	int yDim;
	int value; // 0 - empty, 1,2 - players id

			   //destructor
	~Field() {
		cout << "Destrukcja";
	}
};

class Chequer{
public:
	int xPosition;
	int yPosition;
	int ownerId;

	//constructor
	Chequer(int x, int y, int id) : xPosition(x), yPosition(y), ownerId(id) {
		xPosition = x;
		yPosition = y;
		ownerId = id;
	}
	
	Chequer(){}//default constructor for class Player

};

class Player: public Person { //Player derive from Person
public:
	int id;
	Chequer chequers[12];
	int countChequers;

	Player(int i, string userName, Chequer cheqrs[]): Person(userName) {//constructor derived
		id = i;
		name = userName;
		for (int i = 0; i < 12; i++) {
			chequers[i] = cheqrs[i];
		}
		countChequers = 12;
	}
	
};

void getName(string &name, int i);
void startGame(string name1, string name2);
void initPlayground(int playground[8][8], Player p1, Player p2);
void drawPlayground(int playground[8][8], Player p1, Player p2);
void move(int plgrnd[8][8], Player& player, Player& player2, bool &endGame);
bool canMove(int dimX, int dimY, int dim2X, int dim2Y, int plgrnd[8][8], Player pla, Player pla2);
bool canTouch(int plgrnd[8][8], int dimX, int dimY, Player player);
bool isGameOver(Player p1, Player p2);

int main() {
	string name1, name2;
	getName(name1, 1);
	getName(name2, 2);
	startGame(name1, name2);

	int a; cin >> a;//tricky
}

void getName(string &name, int i) {
	cout << "Graczu"<<i<<", podaj imie: ";
	cin >> name;
	cout << endl;
}

void startGame(string name1, string name2) {
	//create tabs of chequers for both players
	Chequer cheqrs1[12] = { { 0,0,1 },{ 0,2,1 },{ 1,1,1 },{ 2,0,1 },{ 2,2,1 },{ 3,1,1 },{ 4,0,1 },{ 4,2,1 },{ 5,1,1 },{ 6,0,1 },{ 6,2,1 },{ 7,1,1 } };
	Chequer cheqrs2[12] = { { 7,7,2 },{ 7,5,2 },{ 6,6,2 },{ 5,7,2 },{ 5,5,2 },{ 4,6,2 },{ 3,7,2 },{ 3,5,2 },{ 2,6,2 },{ 1,7,2 },{ 1,5,2 },{ 0,6,2 } };

	Player player1(1, name1, cheqrs1);
	Player player2(2, name2, cheqrs2);

	int playground[8][8] = { 0 };
	int currentPlayer = 1;
	initPlayground(playground, player1, player2);
	drawPlayground(playground, player1, player2);

	bool endOfGame = false;
	do {
		if (currentPlayer == 1 && !endOfGame) {
			move(playground, player1, player2, endOfGame);
			currentPlayer = 2;
		}
		if (currentPlayer == 2 && !endOfGame) {
			move(playground, player2, player1, endOfGame);
			currentPlayer = 1;
		}

	} while (!endOfGame);
	cout << "Koniec gry. Wygrywa:"<<endl;
	if (player1.countChequers) cout << player1.name;
	else if (player2.countChequers) cout << player2.name;
}

void initPlayground(int playground[8][8], Player p1, Player p2) {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			for (int k = 0; k < 12; k++) {
				//set value of playground to players' id
				if (p1.chequers[k].xPosition == j && p1.chequers[k].yPosition == i) {
					playground[i][j] = 1;
				}
				if (p2.chequers[k].xPosition == j && p2.chequers[k].yPosition == i) {
					playground[i][j] = 2;
				}
			}
		}
	}
}
void drawPlayground(int playground[8][8], Player p1, Player p2) {
	system("cls");
	for (int i = -1; i<8; i++) {
		
		if (i == -1) {
			cout << "   ";
			for (int j = 0; j<8; j++)
				cout << j + 1 << "  ";
			cout << endl;
		}

		else 
		{
			char a = i + 65;
			cout << a << " ";
			for (int j = 0; j<8; j++) {
				bool canWrite = true;

				if (playground[i][j] == 1) {
					cout << "[*]";
					canWrite = false;
				}

				if (playground[i][j] == 2) {
					cout << "[#]";
					canWrite = false;
				}

				if (canWrite)
					cout << "[ ]";
			}
			cout << "  |  ";
			//scoreboard
			switch (i)
			{
			case 0: {
				if (p1.id == 1)
					cout << p1.name << " " << p1.countChequers;
				else
					cout << p2.name << " " << p2.countChequers;
				break;
			}
			case 1: {
				if (p2.id == 2)
					cout << p2.name << " " << p2.countChequers;
				else
					cout << p1.name << " " << p1.countChequers;
				break;
			}
			default:
				break;
			}

			cout << endl;
		}
	}
}
void move(int plgrnd[8][8], Player& player, Player& player2, bool &endGame) {
	
	string dim, dim2; //dimensions by player

	cout << player.name << " podaj wspolrzedne pionka do ruszenia(np A0)";
	cin >> dim;

	transform(dim.begin(), dim.end(), dim.begin(), ::tolower);//transform tolower

	while (dim.length() != 2 || (dim[0] < 'a' || dim[0]>'h') || dim[1]<'1' || dim[1] >'8') {
		cout << "BLEDNE DANE" << endl;
		cout << player.name << " podaj wspolrzedne pionka do ruszenia(np A0)";
		cin >> dim;
		transform(dim.begin(), dim.end(), dim.begin(), ::tolower);
	}

	int dimX = (dim[0] - 97); //getting dimX from string dim
	int dimY = (dim[1] - 48) - 1; // getting dimY from string dim, -1 cos tab first index is 0, from stream get A1 > dimY = 0

	while (!canTouch(plgrnd, dimX, dimY, player)) {//while u cant touch, u get info
		
		cout << "Nie mozesz ruszyc tego pola" << endl<< 
		player.name << " podaj wspolrzedne pionka do ruszenia(np A0)";

		cin >> dim;
		transform(dim.begin(), dim.end(), dim.begin(), ::tolower); 
		dimX = (dim[0] - 97);
		dimY = (dim[1] - 48) - 1;
	}

	cout << player.name << " podaj nowe wspolrzedne pionka (np A0)";
	cin >> dim2;
	transform(dim2.begin(), dim2.end(), dim2.begin(), ::tolower);

	while (dim2.length() != 2 || (dim2[0] < 'a' || dim2[0]>'h') || dim2[1]<'1' || dim2[1] >'8') {//if unexpected string
		cout << "BLEDNE DANE" << endl;
		cout << player.name << " podaj nowe wspolrzedne pionka (np A0)";
		cin >> dim2;
		transform(dim2.begin(), dim2.end(), dim2.begin(), ::tolower); 
	}

	int dim2X = (dim2[0] - 97);
	int dim2Y = (dim2[1] - 48) - 1;

	while (!canMove(dimX, dimY, dim2X, dim2Y, plgrnd, player, player2)) {//if cant move on dim2X > info
		cout << "Na to pole nie mozesz przejsc"<< 
		player.name << " podaj nowe wspolrzedne pionka (np A0)";
		cin >> dim2;
		transform(dim2.begin(), dim2.end(), dim2.begin(), ::tolower);

		dim2X = (dim2[0] - 97);
		dim2Y = (dim2[1] - 48) - 1;
	}

	//if u attack
	if (abs(dim2X - dimX) > 1) {
		int betweenX = (dimY + dim2Y) / 2;
		int betweenY = (dimX + dim2X) / 2;

		plgrnd[betweenY][betweenX] = 0;
		player.countChequers--; //decrease number of chequers
	}

	plgrnd[dim2X][dim2Y] = plgrnd[dimX][dimY];
	plgrnd[dimX][dimY] = 0;

	if (isGameOver(player, player2))
		endGame = true;
	drawPlayground(plgrnd, player, player2);
}
bool canTouch(int plgrnd[8][8], int dimX, int dimY, Player player) {
	if (plgrnd[dimX][dimY] != player.id) return false;
	return true;
}
bool canMove(int dimX, int dimY, int dim2X, int dim2Y, int plgrnd[8][8], Player pla, Player pla2) {

	//dimX - from
	//dim2X - to
	//betweenX - from | betweenX | to
	int betweenX = 0;
	int betweenY = 0;

	{
		//if want to kill
		if (abs(dim2X - dimX) > 1) {
			betweenX = (dimX + dim2X) / 2;
			betweenY = (dimY + dim2Y) / 2;

			//checking if field is my or not
			if (plgrnd[betweenX][betweenY] == pla.id || plgrnd[betweenX][betweenY] == pla2.id) {
				if (plgrnd[betweenX][betweenY] != plgrnd[dimX][dimY])
					return true;
				return false;
			}
			return false;
		}
		if (abs(dim2Y - dimY) > 1) {

			betweenX = (dimX + dim2X) / 2;
			betweenY = (dimY + dim2Y) / 2;

			if (plgrnd[betweenX][betweenY] == pla.id || plgrnd[betweenX][betweenY] == pla2.id) {
				if (plgrnd[betweenX][betweenY] != plgrnd[dimX][dimY])
					return true;
				return false;
			}
			return false;
		}

		//if want move on line
		if (abs(dim2X - dimX) == 0 || abs(dim2Y - dimY) == 0)
			return false;
		//if field "to" is not empty
		if (plgrnd[dim2X][dim2Y])
			return false;

		//else
		return true;
	}


}
bool isGameOver(Player p1, Player p2) {
	if (p1.countChequers && p2.countChequers) return false;
	return true;
}
